use std::fmt;

use tokio_core::reactor::Handle;
use futures::{Future, Poll, Sink, Stream};

use hyper;
use hyper::{Body, Method, Request, StatusCode};
use hyper::client::HttpConnector;
use hyper::header::{Authorization, ContentLength, ContentType, UserAgent};
use hyper_tls::HttpsConnector;

use websocket::{ClientBuilder, OwnedMessage};

use serde::Serialize;
use serde::de::DeserializeOwned;
use serde_json;

use error::DiscordError;

#[must_use = "futures do nothing unless polled"]
pub struct DiscordResponse<V>(Box<Future<Item = V, Error = DiscordError> + 'static>);
impl<V> Future for DiscordResponse<V> {
	type Item = V;
	type Error = DiscordError;

	fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
		self.0.poll()
	}
}

#[must_use = "streams do nothing unless polled"]
pub struct WebSocketResult(Box<Future<Item = (), Error = DiscordError> + 'static>);
impl Future for WebSocketResult {
	type Item = ();
	type Error = DiscordError;

	fn poll(&mut self) -> Poll<Self::Item, Self::Error> {
		self.0.poll()
	}
}

pub enum AuthType {
	Bot,
	Bearer,
}
impl fmt::Display for AuthType {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match self {
			&AuthType::Bot => write!(f, "Bot"),
			&AuthType::Bearer => write!(f, "Bearer"),
		}
	}
}

pub struct Client {
	authorization: String,
	api_root: String,

	core_handle: Handle,
	http: hyper::Client<HttpsConnector<HttpConnector>, Body>,
}

impl Client {
	pub fn new(token_type: AuthType, token: &str, core: Handle) -> Client {
		let http = hyper::Client::configure()
			.connector(HttpsConnector::new(4, &core).unwrap())
			.build(&core);
		Client {
			authorization: format!("{} {}", token_type, token),
			api_root: "https://discordapp.com/api".to_string(),

			core_handle: core,
			http: http,
		}
	}

	pub fn set_api_root(&mut self, root: &str) {
		self.api_root = root.to_string();
	}

	pub fn leak_token(&self) {
		println!("{}", self.authorization);
	}

	fn get<T>(&self, path: String) -> DiscordResponse<T>
	where
		T: 'static + DeserializeOwned,
	{
		let uri = format!("{}{}", self.api_root, path).parse().unwrap();
		let mut request = Request::new(Method::Get, uri);
		{
			let headers = request.headers_mut();
			headers.set(Authorization(self.authorization.clone()));
			headers.set(UserAgent::new(
				"DiscordBot (https://gitlab.com/rytone/discordlib, 0.1.0)",
			));
		}

		return DiscordResponse(Box::new(
			self.http
				.request(request)
				.map_err(DiscordError::NetworkError)
				.and_then(|res| match res.status() {
					StatusCode::Ok => Ok(res),
					_ => Err(DiscordError::NetStatusError(res.status())),
				})
				.and_then(|res| {
					res.body().concat2().map_err(DiscordError::NetworkError)
				})
				.and_then(|body| {
					serde_json::from_slice(&body).map_err(DiscordError::JSONError)
				}),
		));
	}

	fn post<A, T>(&self, path: String, req_body: A) -> DiscordResponse<T>
	where
		A: Serialize,
		T: 'static + DeserializeOwned,
	{
		let req_body = serde_json::to_vec(&req_body).unwrap(); // TODO remove unwrap

		let uri = format!("{}{}", self.api_root, path).parse().unwrap();

		let mut request = Request::new(Method::Post, uri);
		{
			let headers = request.headers_mut();
			headers.set(Authorization(self.authorization.clone()));
			headers.set(ContentLength(req_body.len() as u64));
			headers.set(ContentType::json());
			headers.set(UserAgent::new(
				"DiscordBot (https://gitlab.com/rytone/discordlib, 0.1.0)",
			));
		}
		request.set_body(req_body);

		return DiscordResponse(Box::new(
			self.http
				.request(request)
				.map_err(DiscordError::NetworkError)
				.and_then(|res| match res.status() {
					StatusCode::Ok => Ok(res),
					_ => Err(DiscordError::NetStatusError(res.status())),
				})
				.and_then(|res| {
					res.body().concat2().map_err(DiscordError::NetworkError)
				})
				.and_then(|body| {
					serde_json::from_slice(&body).map_err(DiscordError::JSONError)
				}),
		));
	}

	fn patch<A, T>(&self, path: String, req_body: A) -> DiscordResponse<T>
	where
		A: Serialize,
		T: 'static + DeserializeOwned,
	{
		let req_body = serde_json::to_vec(&req_body).unwrap(); // TODO remove unwrap

		let uri = format!("{}{}", self.api_root, path).parse().unwrap();

		let mut request = Request::new(Method::Patch, uri);
		{
			let headers = request.headers_mut();
			headers.set(Authorization(self.authorization.clone()));
			headers.set(ContentLength(req_body.len() as u64));
			headers.set(ContentType::json());
			headers.set(UserAgent::new(
				"DiscordBot (https://gitlab.com/rytone/discordlib, 0.1.0)",
			));
		}
		request.set_body(req_body);

		return DiscordResponse(Box::new(
			self.http
				.request(request)
				.map_err(DiscordError::NetworkError)
				.and_then(|res| match res.status() {
					StatusCode::Ok => Ok(res),
					_ => Err(DiscordError::NetStatusError(res.status())),
				})
				.and_then(|res| {
					res.body().concat2().map_err(DiscordError::NetworkError)
				})
				.and_then(|body| {
					serde_json::from_slice(&body).map_err(DiscordError::JSONError)
				}),
		));
	}

	pub fn get_guild_text_channel(
		&self,
		id: String,
	) -> DiscordResponse<::objects::GuildTextChannel> {
		self.get(format!("/channels/{}", id))
	}
	pub fn get_voice_channel(&self, id: String) -> DiscordResponse<::objects::VoiceChannel> {
		self.get(format!("/channels/{}", id))
	}
	pub fn get_dm_channel(&self, id: String) -> DiscordResponse<::objects::DMChannel> {
		self.get(format!("/channels/{}", id))
	}

	pub fn send_message(
		&self,
		channel_id: String,
		content: String,
		tts: bool,
	) -> DiscordResponse<::objects::Message> {
		#[derive(Serialize)]
		struct MessageParams {
			content: String,
			tts: bool,
		};
		self.post(
			format!("/channels/{}/messages", channel_id),
			MessageParams {
				content: content,
				tts: tts,
			},
		)
	}

	pub fn edit_message(
		&self,
		channel_id: String,
		message_id: String,
		content: String,
	) -> DiscordResponse<::objects::Message> {
		#[derive(Serialize)]
		struct EditParams {
			content: String,
		};
		self.patch(
			format!("/channels/{}/messages/{}", channel_id, message_id),
			EditParams { content: content },
		)
	}

	fn gateway(&self) -> DiscordResponse<::objects::GatewayResponse> {
		self.get("/gateway".to_string())
	}

	pub fn connect(&self) -> WebSocketResult {
		let handle = self.core_handle.clone();
		WebSocketResult(Box::new(
			self.gateway()
				.map(|s| format!("{}/?v=6&encoding=json", s.url))
				.and_then(move |url| {
					ClientBuilder::new(&url)
						.unwrap()
						.async_connect_secure(None, &handle)
						.map_err(DiscordError::WebSocketError)
				})
				.and_then(|(duplex, _)| {
					let (sink, stream) = duplex.split();
					stream
						.filter_map(|m| match m {
							OwnedMessage::Text(m) => {
								println!("WebSocket message: {}", m);
								None
							}
							_ => None,
						})
						.map_err(DiscordError::WebSocketError)
						.forward(sink.sink_map_err(DiscordError::WebSocketError))
						.map(|_| ())
				}),
		))
	}
}
