#[derive(Deserialize)]
pub struct GuildTextChannel {
	pub id: String,
	pub guild_id: String,
	pub position: i32,
	// TODO perm overwrites
	pub name: String,
	pub topic: Option<String>,
	pub last_message_id: Option<String>,
	pub icon: Option<String>,
}

#[derive(Deserialize)]
// TODO break out into DMChannel and GroupDMChannel?
pub struct DMChannel {
	pub id: String,
	pub name: Option<String>,
	pub owner_id: Option<String>,
	pub last_message_id: Option<String>,
	pub icon: Option<String>,
	// TODO recipients (this makes it incompatible with other channel types)
}

#[derive(Deserialize)]
pub struct VoiceChannel {
	pub id: String,
	pub guild_id: String,
	pub position: i32,
	// TODO perm overwrites
	pub name: String,
}