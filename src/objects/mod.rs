mod channel;
mod message;

pub use self::channel::GuildTextChannel;
pub use self::channel::VoiceChannel;
pub use self::channel::DMChannel;

pub use self::message::Message;

#[derive(Deserialize)]
pub struct GatewayResponse {
	pub url: String,
}
