use std::fmt;
use std::error;

use hyper;
use hyper::StatusCode;

use websocket::result::WebSocketError;

use serde_json;

#[derive(Debug)]
pub enum DiscordError {
	NetworkError(hyper::Error),
	NetStatusError(StatusCode),
	JSONError(serde_json::Error),
	WebSocketError(WebSocketError),
}

impl fmt::Display for DiscordError {
	fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
		match self {
			&DiscordError::NetworkError(ref err) => write!(f, "NetworkError: {}", err),
			&DiscordError::NetStatusError(ref code) => write!(f, "NetStatusError: {}", code),
			&DiscordError::JSONError(ref err) => write!(f, "JSONError: {}", err),
			&DiscordError::WebSocketError(ref err) => write!(f, "WebSocketError: {}", err),
		}
	}
}

impl error::Error for DiscordError {
	fn description(&self) -> &str {
		match self {
			&DiscordError::NetworkError(ref err) => err.description(),
			&DiscordError::NetStatusError(ref code) => code.canonical_reason().unwrap(), // TODO remove unwrap
			&DiscordError::JSONError(ref err) => err.description(),
			&DiscordError::WebSocketError(ref err) => err.description(),
		}
	}

	fn cause(&self) -> Option<&error::Error> {
		match self {
			&DiscordError::NetworkError(ref err) => Some(err),
			&DiscordError::NetStatusError(_) => None,
			&DiscordError::JSONError(ref err) => Some(err),
			&DiscordError::WebSocketError(ref err) => Some(err),
		}
	}
}