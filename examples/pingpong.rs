extern crate discordlib;
extern crate tokio_core;
extern crate futures;
extern crate hyper;
extern crate websocket;

use std::time::Instant;

use tokio_core::reactor::Core;
use futures::{Future, Stream};

use websocket::OwnedMessage;

use discordlib::client::{AuthType, Client};

fn main() {
	let token = std::env::args().nth(1).expect("token required");

	let mut core = Core::new().unwrap();

	let client = Client::new(AuthType::Bot, &token, core.handle());
	client.leak_token();

	let t = Instant::now();
	let work = client
		.send_message("326128172825444353".to_string(), "...".to_string(), false)
		.and_then(|m| {
			let elapsed = t.elapsed();
			let nanos = elapsed.subsec_nanos() as u64;
			let ms = (1000 * 1000 * 1000 * elapsed.as_secs() + nanos) / (1000 * 1000);
			client.edit_message(
				"326128172825444353".to_string(),
				m.id,
				format!("ping: {}ms", ms),
			)
		})
		.and_then(|m| {
			println!("Complete!\nMessage ID: {}", m.id);
			client.connect()
		})
		.map_err(|err| println!("ERROR: {}", err));

	core.run(work).unwrap();
}
